Smart List Generator
====================
My first steps in Scala...

Introduction
------------
The goal of this little project is to generate a list from its firsts elements. For example : 
If the input is : 

```
2 4 8
```

the output will be :

```
2 - 4 - 8 - 16 - 32 - 64 - 128 - 256 - 512 - 1024 - 2048 - 4096 - 8192 - ...
```

Another example : 
Input : 

```
2 6
```

Output : 

```
2 - 6 - 10 - 14 - 18 - 22 - 26 - 30 - 34 - 38 - 42 - 46 - ...
```

Build
-----
To build this project you can use the makefile :
```
make
```

To Build and Run :
```
make run
```

