import java.util.Scanner

object App {
	def main(args: Array[String]): Unit = {
		println("Veuillez saisir le début d'une liste : ")
		val sc : Scanner = new Scanner(System.in)
		val input = sc.nextLine()
		// On créer la liste de nombre à partir de la chaine de caractères
		val tab = input.split(" ").map(_.toInt)
		println(affTab(genererSuite(tab)) + " - ... ")
	}
	
	def genererSuite(list: Array[Int]) : Array[Int] = {
		// Il faut au moins deux termes pour pouvoir créer la suite
		if(list.length >= 2){
			if(isSuiteAdd(list)){
				println("Suite construite grace a l'addition")
				// Calcul de l'offset (soit un terme, l'offset est l'écart entre son suivant et lui-même)
				var offset = list(1) - list(0)
				/*
				Créér la liste (méthode addition)
				x      -> premier terme
				offset -> soit un terme, l'offset est l'écart entre son suivant et lui-même
				it     -> nombre de nombre à générer en plus de ceux déjà existants
				*/
				def add(x : Int, offset : Int, it : Int) : Array[Int] = {
					if(it <= 0) return Array()
					Array(x + offset) ++ add(x + offset, offset, it - 1)	
				}
				return list ++ add(list.last, offset, 10)
			}else{
				println("Suite construite grace a la multiplication")
				// Calcul de l'offset (soit un terme, l'offset est l'écart entre son suivant et lui-même)
				var offset = list(1) / list(0)
				/*
				Créér la liste (méthode mutiliplication)
				x      -> premier terme
				offset -> soit un terme, l'offset est l'écart entre son suivant et lui-même
				it     -> nombre de nombre à générer en plus de ceux déjà existants
				*/
				def mult(x : Int, offset : Int, it : Int) : Array[Int] = {
					if(it <= 0) return Array()
					Array(x * offset) ++ mult(x * offset, offset, it - 1)					
				}
				return list ++ mult(list.last, offset, 10)
			}
		}
		println("L'entree n'est pas correcte !")
		Array()
	}


	/*
	Le toString d'une liste
	*/
	def affTab(list: Array[Int]) : String = {
		if(list.tail.length == 0) return list.head.toString
		list.head + " - " + affTab(list.tail)
	}

	/*
	Détermine si une liste est construite par l'addition
	list -> liste a partir de laquelle on détermine si le reste de la suite est définie par l'addition	
	*/
	def isSuiteAdd(list: Array[Int]) : Boolean = {
		// Si la longueur de la liste est de 2, elle est forcément construite par l'addition
		if(list.length <= 2){
			true
		}else{
			def parcours(list: Array[Int]) : Array[Int] = {
				if(list.tail.length == 0) return Array()
				
				Array(list.tail.head - list.head) ++  parcours(list.tail)
			}
			parcours(list).reduceLeft(_ - _) == 0
		}
		
	}
}